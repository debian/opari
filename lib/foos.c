#include <stdio.h>

void   foo()   { puts("#define FSUB(name) name"); }
void   foo_()  { puts("#define FSUB(name) SUFFIX(name)"); }
void   foo__() { puts("#define FSUB(name) SUFFIX2(name)"); }
void  _foo()   { puts("#define FSUB(name) PREFIX(name)"); }
void __foo()   { puts("#define FSUB(name) PREFIX2(name)"); }
void  _foo_()  { puts("#define FSUB(name) PREFIX(SUFFIX(name))"); }
void   FOO()   { puts("#define FSUB(name) UPCASE(name)"); }
void   FOO_()  { puts("#define FSUB(name) SUFFIX(UPCASE(name))"); }
void   FOO__() { puts("#define FSUB(name) SUFFIX2(UPCASE(name))"); }
void  _FOO()   { puts("#define FSUB(name) PREFIX(UPCASE(name))"); }
void __FOO()   { puts("#define FSUB(name) PREFIX2(UPCASE(name))"); }
void  _FOO_()  { puts("#define FSUB(name) PREFIX(SUFFIX(UPCASE(name)))"); }
