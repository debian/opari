! /*************************************************************************/
! /* OPARI Version 1.1                                                     */
! /* Copyright (C) 2001                                                    */
! /* Forschungszentrum Juelich, Zentralinstitut fuer Angewandte Mathematik */
! /*************************************************************************/

       SUBROUTINE POMP_INIT_LOCK(LOCK)
         POINTER (LOCK,IL)
         INTEGER IL
         EXTERNAL OMP_INIT_LOCK
         CALL OMP_INIT_LOCK(LOCK)
         RETURN
       END

       SUBROUTINE POMP_INIT_NEST_LOCK(NLOCK)
         POINTER (NLOCK,NIL)
         INTEGER NIL
         EXTERNAL OMP_INIT_NEST_LOCK
         CALL OMP_INIT_NEST_LOCK(NLOCK)
         RETURN
       END

       SUBROUTINE POMP_DESTROY_LOCK(LOCK)
         POINTER (LOCK,IL)
         INTEGER IL
         EXTERNAL OMP_DESTROY_LOCK
         CALL OMP_DESTROY_LOCK(LOCK)
         RETURN
       END

       SUBROUTINE POMP_DESTROY_NEST_LOCK(NLOCK)
         POINTER (NLOCK,NIL)
         INTEGER NIL
         EXTERNAL OMP_DESTROY_NEST_LOCK
         CALL OMP_DESTROY_NEST_LOCK(NLOCK)
         RETURN
       END

       SUBROUTINE POMP_SET_LOCK(LOCK)
         POINTER (LOCK,IL)
         INTEGER IL
         EXTERNAL OMP_SET_LOCK
         CALL OMP_SET_LOCK(LOCK)
         RETURN
       END

       SUBROUTINE POMP_SET_NEST_LOCK(NLOCK)
         POINTER (NLOCK,NIL)
         INTEGER NIL
         EXTERNAL OMP_SET_NEST_LOCK
         CALL OMP_SET_NEST_LOCK(NLOCK)
         RETURN
       END

       SUBROUTINE POMP_UNSET_LOCK(LOCK)
         POINTER (LOCK,IL)
         INTEGER IL
         EXTERNAL OMP_UNSET_LOCK
         CALL OMP_UNSET_LOCK(LOCK)
         RETURN
       END

       SUBROUTINE POMP_UNSET_NEST_LOCK(NLOCK)
         POINTER (NLOCK,NIL)
         INTEGER NIL
         EXTERNAL OMP_UNSET_NEST_LOCK
         CALL OMP_UNSET_NEST_LOCK(NLOCK)
         RETURN
       END

       LOGICAL FUNCTION POMP_TEST_LOCK(LOCK)
         POINTER (LOCK,IL)
         INTEGER IL
         EXTERNAL OMP_TEST_LOCK
         LOGICAL OMP_TEST_LOCK
         POMP_TEST_LOCK = OMP_TEST_LOCK(LOCK)
         RETURN
       END

       INTEGER FUNCTION POMP_TEST_NEST_LOCK(NLOCK)
         POINTER (NLOCK,NIL)
         INTEGER NIL
         EXTERNAL OMP_TEST_NEST_LOCK
         INTEGER OMP_TEST_NEST_LOCK
         POMP_TEST_NEST_LOCK = OMP_TEST_NEST_LOCK(NLOCK)
         RETURN
       END
